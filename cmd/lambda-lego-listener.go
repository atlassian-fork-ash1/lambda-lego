package main

import (
	"bytes"
	"context"
	"log"

	"bitbucket.org/atlassian/lambda-lego/internal/cfg"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

// FulfillChallenge responds to a request from an ACME server, pulls down the challenge token that was placed in s3 by lambda-lego
// and returns it.
func FulfillChallenge(ctx context.Context, request events.ALBTargetGroupRequest) (events.ALBTargetGroupResponse, error) {
	llcfg := cfg.New()

	log.Println("Getting AWS session")
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(llcfg.GetRegion())},
	)
	if err != nil {
		log.Println("Error creating AWS session")
		log.Fatal(err)
		return events.ALBTargetGroupResponse{
			StatusCode: 503,
		}, err
	}

	s3sess := s3.New(sess)

	input := &s3.GetObjectInput{
		Bucket: aws.String(llcfg.GetBucketName()),
		Key:    aws.String(llcfg.GetAcmeChallengeS3Key()),
	}

	log.Println("Getting challenge from s3")
	result, err := s3sess.GetObject(input)
	if err != nil {
		log.Printf("Error retrieving challenge from s3")
		log.Fatal(err)
		return events.ALBTargetGroupResponse{
			StatusCode: 503,
		}, err
	}

	log.Println("Got challenge from s3")
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(result.Body)
	if err != nil {
		log.Printf("Error reading response body from s3")
		log.Fatal(err)
		return events.ALBTargetGroupResponse{
			StatusCode: 503,
		}, err
	}
	challenge := buf.String()
	headers := make(map[string]string)
	headers["Content-Type"] = "text/plain"

	log.Printf("Responding to challenge request with %s\n", challenge)
	return events.ALBTargetGroupResponse{
		StatusCode:        200,
		StatusDescription: "OK",
		Headers:           headers,
		Body:              challenge,
		IsBase64Encoded:   false,
	}, nil
}

func main() {
	lambda.Start(FulfillChallenge)
}
