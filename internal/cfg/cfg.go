package cfg

import (
	"log"
	"os"
	"strconv"
)

// LambdaLegoConfig is an object containing configuration for lambda-lego
type LambdaLegoConfig struct {
	acmeUserKeyID         string
	adminEmail            string
	albArn                string
	bucket                string
	certAuthorityEndpoint string
	certS3Key             string
	clusterHTTPSPort      string
	clusterTargetGroup    string
	challengeKey          string
	domain                string
	dryRun                bool
	region                string
}

// New populates the config object from the environment
func New() LambdaLegoConfig {
	return LambdaLegoConfig{
		acmeUserKeyID:         os.Getenv("ACME_USER_SM_KID"),
		adminEmail:            os.Getenv("ADMIN_EMAIL"),
		albArn:                os.Getenv("ALB_ARN"),
		bucket:                os.Getenv("CERT_BUCKET"),
		certAuthorityEndpoint: os.Getenv("CA_ENDPOINT"),
		certS3Key:             os.Getenv("CERT_S3_KEY"),
		clusterHTTPSPort:      os.Getenv("CLUSTER_HTTPS_PORT"),
		clusterTargetGroup:    os.Getenv("CLUSTER_TARGET_GROUP_ARN"),
		challengeKey:          os.Getenv("ACME_CHALLENGE_S3_KEY"),
		domain:                os.Getenv("CERT_DOMAIN"),
		dryRun:                os.Getenv("DRY_RUN") == "true",
		region:                os.Getenv("AWS_LAMBDA_REGION"),
	}
}

// GetDomain returns the domain to acquire a certificate for
func (cfg LambdaLegoConfig) GetDomain() string {
	return cfg.domain
}

// GetBucketName returns the name of the bucket to store certificate data in
func (cfg LambdaLegoConfig) GetBucketName() string {
	return cfg.bucket
}

// GetAdminEmail returns the email address of the admin responsible for maintaining the certificate
func (cfg LambdaLegoConfig) GetAdminEmail() string {
	return cfg.adminEmail
}

// GetCertificateAuthorityEndpoint returns the URL of the ACME api endpoint to request a certificate from
func (cfg LambdaLegoConfig) GetCertificateAuthorityEndpoint() string {
	return cfg.certAuthorityEndpoint
}

// GetRegion returns the AWS Region to create the s3 bucket and deploy the certificate to ACM in
func (cfg LambdaLegoConfig) GetRegion() string {
	return cfg.region
}

// GetAcmeChallengeS3Key returns the key to store the ACME challenge token with in S3
func (cfg LambdaLegoConfig) GetAcmeChallengeS3Key() string {
	return cfg.challengeKey
}

// GetACMEUserKeyID returns they identifier to store the acme user private key against in aws secrets manager
func (cfg LambdaLegoConfig) GetACMEUserKeyID() string {
	return cfg.acmeUserKeyID
}

// IsDryRun returns a flag indicating whether or not to actually request a certificate
func (cfg LambdaLegoConfig) IsDryRun() bool {
	return cfg.dryRun
}

// GetCertificateS3Key returns the key to use to store the certificate in s3
func (cfg LambdaLegoConfig) GetCertificateS3Key() string {
	return cfg.certS3Key
}

// GetClusterTargetGroup returns the ARN of the ALB target group containing the cluster to secure
func (cfg LambdaLegoConfig) GetClusterTargetGroup() string {
	return cfg.clusterTargetGroup
}

// GetALB returns the ARN of the ALB to attach the certificate to
func (cfg LambdaLegoConfig) GetALB() string {
	return cfg.albArn
}

// GetClusterHTTPSPort returns the port of cluster instances to forward traffic to
func (cfg LambdaLegoConfig) GetClusterHTTPSPort() int64 {
	port, err := strconv.ParseInt(cfg.clusterHTTPSPort, 10, 64)
	if err != nil {
		log.Println("Error parsing port config")
		log.Fatal(err)
	}
	return port
}
