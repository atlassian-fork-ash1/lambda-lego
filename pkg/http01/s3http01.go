package http01

import (
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

// ProviderS3 implements ChallengeProvider for `http-01` challenge
// It uploads the challenge to s3
type ProviderS3 struct {
	bucket string
	key    string
	region string
}

// NewProviderS3 creates a ProviderS3 with the specified properties
func NewProviderS3(bucket, key, region string) *ProviderS3 {
	return &ProviderS3{
		bucket: bucket,
		key:    key,
		region: region,
	}
}

// Present creates a challenge and uploads it to s3
func (p *ProviderS3) Present(domain, token, keyauth string) error {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(p.region)},
	)

	if err != nil {
		return err
	}

	// Create S3 service client
	s3sess := s3.New(sess)

	var tagBuilder strings.Builder
	tagBuilder.WriteString("IssuedAt=")
	tagBuilder.WriteString(time.Now().Format("01-02-2006 15:04:05"))
	tagBuilder.WriteString("&atlCertDomain=")
	tagBuilder.WriteString(domain)

	putInput := &s3.PutObjectInput{
		Body:    strings.NewReader(keyauth),
		Bucket:  aws.String(p.bucket),
		Key:     aws.String(p.key),
		Tagging: aws.String(tagBuilder.String()),
	}

	_, err = s3sess.PutObject(putInput)
	if err != nil {
		return err
	}

	return nil
}

// CleanUp removes the old challenge from s3
func (p *ProviderS3) CleanUp(domain, token, keyauth string) error {
	return nil
}
